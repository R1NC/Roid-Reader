package com.rincliu.xreader.util;

import android.content.Context;

public class SysUtil {
    /**
     * @param context
     * @param dp
     * @return
     */
    public static int dip2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}
