package com.rincliu.xreader.data.model.rss;

import java.io.Serializable;

public class RSSSource implements Serializable {
    private static final long serialVersionUID = -5088735156991194029L;

    private String url;

    private String desc;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
