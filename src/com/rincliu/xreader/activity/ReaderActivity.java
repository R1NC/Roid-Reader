package com.rincliu.xreader.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rincliu.xreader.R;
import com.rincliu.xreader.data.RSSReader;
import com.rincliu.xreader.data.RSSReader.ReadCallback;
import com.rincliu.xreader.data.db.ReaderDBHelper;
import com.rincliu.xreader.data.model.rss.RSSData;
import com.rincliu.xreader.data.model.rss.RSSEntity;
import com.rincliu.xreader.data.model.rss.RSSSource;
import com.rincliu.xreader.util.SysUtil;
import com.rincliu.xreader.util.UiUtil;
import com.rincliu.xreader.view.sliding.SlidingActivity;
import com.rincliu.xreader.view.sliding.SlidingMenu;
import com.rincliu.xreader.view.sliding.SlidingMenu.OnCloseListener;

public class ReaderActivity extends SlidingActivity {
    public static final String EXTRA_SOURCE_LIST = "EXTRA_SOURCE_LIST";

    public static final String EXTRA_DATA = "EXTRA_DATA";

    private ArrayList<RSSSource> sourceList;

    private ArrayList<RSSEntity> dataList;

    private ListView lvRss;

    private BaseAdapter rssAdapter, sourceAdapter;

    private ReaderDBHelper rdbh;

    private SourceContentObserver sourceObserver;

    private boolean isExit;

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        rdbh = ReaderDBHelper.getInstance(this);
        sourceObserver = new SourceContentObserver(getHandler());
        sourceList = (ArrayList<RSSSource>) getIntent().getSerializableExtra(EXTRA_SOURCE_LIST);
        RSSData data = (RSSData) getIntent().getSerializableExtra(EXTRA_DATA);
        if (data != null) {
            dataList = data.getItemList();
            if (!data.isCache()) {
                UiUtil.toast(ReaderActivity.this,
                        String.format(getString(R.string.found_some_new_articles), data.getItemList().size()), true);
            }
        }

        setContentView(R.layout.activity_reader);
        if (sourceList != null && sourceList.size() > 0) {
            setTitle(sourceList.get(0).getDesc());
        }

        setBehindContentView(getSourceMenu());
        getSlidingMenu().setMode(SlidingMenu.LEFT_RIGHT);
        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        getSlidingMenu().setSecondaryMenu(getSettingMenu());
        getSlidingMenu().setBehindOffset(SysUtil.dip2px(this, 50));
        getSlidingMenu().setFadeEnabled(true);
        getSlidingMenu().setShadowWidth(SysUtil.dip2px(this, 10));
        getSlidingMenu().setShadowDrawable(getResources().getDrawable(R.drawable.slide_shadow_left));
        getSlidingMenu().setSecondaryShadowDrawable(getResources().getDrawable(R.drawable.slide_shadow_right));
        getSlidingMenu().setOnCloseListener(new OnCloseListener() {
            @Override
            public void onClose() {
                sourceAdapter.notifyDataSetChanged();
            }
        });

        lvRss = (ListView) findViewById(R.id.lv_rss);
        rssAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return dataList == null ? 0 : dataList.size();
            }

            @Override
            public Object getItem(int position) {
                return dataList == null ? null : dataList.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View view, ViewGroup parent) {
                if (dataList == null) {
                    return null;
                }
                final RSSEntity item = dataList.get(position);
                ViewHolder holder;
                if (view == null) {
                    view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.rss_list_item, null);
                    holder = new ViewHolder();
                    holder.tvTitle = (TextView) view.findViewById(R.id.tv_title);
                    holder.tvSummary = (TextView) view.findViewById(R.id.tv_summary);
                    holder.tvDate = (TextView) view.findViewById(R.id.tv_date);
                    view.setTag(holder);
                } else {
                    holder = (ViewHolder) view.getTag();
                }
                holder.tvTitle.setText(item.getTitle());
                holder.tvSummary.setText(Html.fromHtml(item.getSummary()));
                holder.tvDate.setText(item.getDate());
                return view;
            }

            class ViewHolder {
                TextView tvTitle, tvSummary, tvDate;
            }
        };
        lvRss.setAdapter(rssAdapter);
        lvRss.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ReaderActivity.this, DetailActivity.class);
                intent.putExtra(DetailActivity.EXTRA_DATA, dataList.get(position));
                startActivity(intent);
            }
        });
        findViewById(R.id.iv_source).setOnClickListener(this);
        findViewById(R.id.iv_setting).setOnClickListener(this);
    }

    private void readSource(final RSSSource source) {
        findViewById(R.id.pb_loading).setVisibility(View.VISIBLE);
        RSSReader.getInstance(this).read(source.getUrl(), new ReadCallback() {
            @Override
            public void onSuccess(RSSData data) {
                findViewById(R.id.pb_loading).setVisibility(View.GONE);
                if (dataList != null) {
                    dataList.clear();
                }
                dataList.addAll(data.getItemList());
                rssAdapter.notifyDataSetChanged();
                lvRss.setSelection(0);
                setTitle(source.getDesc());
                if (!data.isCache()) {
                    UiUtil.toast(ReaderActivity.this,
                            String.format(getString(R.string.found_some_new_articles), data.getItemList().size()), true);
                }
                data.getItemList().clear();
            }

            @Override
            public void onFailure() {
                findViewById(R.id.pb_loading).setVisibility(View.GONE);
                UiUtil.toast(getApplicationContext(), R.string.read_data_failed, true);
            }
        });
    }

    private View getSourceMenu() {
        View view = LayoutInflater.from(this).inflate(R.layout.source_list, null);
        ListView lvSource = (ListView) view.findViewById(R.id.lv_source);
        sourceAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return sourceList == null ? 0 : sourceList.size();
            }

            @Override
            public Object getItem(int position) {
                return sourceList == null ? null : sourceList.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                ViewHolder holder;
                if (convertView == null) {
                    convertView = LayoutInflater.from(ReaderActivity.this).inflate(R.layout.source_list_item, null);
                    holder = new ViewHolder();
                    holder.tvSource = (TextView) convertView.findViewById(R.id.tv_source);
                    holder.ivDelete = (ImageView) convertView.findViewById(R.id.iv_delete);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                holder.tvSource.setText(sourceList.get(position).getDesc());
                holder.ivDelete.setVisibility(View.GONE);
                holder.ivDelete.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = sourceList.get(position).getUrl();
                        rdbh.removeSource(url);
                        rdbh.removeData(url);
                        sourceAdapter.notifyDataSetChanged();
                    }
                });
                return convertView;
            }

            class ViewHolder {
                TextView tvSource;

                ImageView ivDelete;
            }
        };
        lvSource.setAdapter(sourceAdapter);
        lvSource.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showContent();
                readSource(sourceList.get(position));
            }
        });
        lvSource.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                view.findViewById(R.id.iv_delete).setVisibility(View.VISIBLE);
                return true;
            }
        });
        final EditText etSource = (EditText) view.findViewById(R.id.et_source);
        final ImageView ivClear = (ImageView) view.findViewById(R.id.iv_clear);
        final ImageView ivAdd = (ImageView) view.findViewById(R.id.iv_add);
        ivAdd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReaderActivity.this, AddSourceActivity.class);
                intent.putExtra(AddSourceActivity.EXTRA_SOURCE_DESC, etSource.getText().toString().trim());
                startActivity(intent);
            }
        });
        ivClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                etSource.setText("");
            }
        });
        etSource.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                String keyWords = etSource.getText().toString();
                if (TextUtils.isEmpty(keyWords)) {
                    ivClear.setVisibility(View.GONE);
                } else {
                    ivClear.setVisibility(View.VISIBLE);
                }
                boolean isAdd = sourceList == null || sourceList.size() == 0;
                ivAdd.setVisibility(isAdd ? View.VISIBLE : View.GONE);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                String keyWords = etSource.getText().toString();
                ArrayList<RSSSource> tmpSourceList = rdbh.getSourceList(keyWords.trim());
                if (sourceList != null) {
                    sourceList.clear();
                }
                sourceList = tmpSourceList;
                sourceAdapter.notifyDataSetChanged();
            }
        });
        return view;
    }

    private View getSettingMenu() {
        View view = LayoutInflater.from(this).inflate(R.layout.setting_list, null);
        view.findViewById(R.id.tv_share).setOnClickListener(this);
        view.findViewById(R.id.tv_rate).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.iv_source:
                showMenu();
                break;
            case R.id.iv_setting:
                showSecondaryMenu();
                break;
            case R.id.tv_share:
                showContent();
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_app_text));
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_app_title));
                intent.setType("text/plain");
                startActivity(Intent.createChooser(intent, getString(R.string.share_this_app)));
                break;
            case R.id.tv_rate:
                showContent();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        rdbh.registerSourceContentObserver(sourceObserver);
    }

    @Override
    public void onPause() {
        super.onPause();
        rdbh.unregisterContentObserver(sourceObserver);
    }

    private class SourceContentObserver extends ContentObserver {
        public SourceContentObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(final boolean selfChange) {
            sourceList = rdbh.getSourceList(null);
            getHandler().sendEmptyMessage(MSG_UPDATE_SOURCE);
        }
    }

    private static final int MSG_UPDATE_SOURCE = 0x9999;

    @Override
    public void onHandleMessage(Message msg) {
        switch (msg.what) {
            case MSG_UPDATE_SOURCE:
                sourceAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (isExit) {
            exit();
        } else {
            UiUtil.toast(this, R.string.press_again_to_exit);
            isExit = true;
            getHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isExit = false;
                }
            }, 2000);
        }
    }

    private void exit() {
        super.onBackPressed();
        //TODO
    }
}
