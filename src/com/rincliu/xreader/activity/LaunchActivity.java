package com.rincliu.xreader.activity;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.rincliu.xreader.R;
import com.rincliu.xreader.app.BaseActivity;
import com.rincliu.xreader.data.RSSReader;
import com.rincliu.xreader.data.RSSReader.ReadCallback;
import com.rincliu.xreader.data.db.ReaderDBHelper;
import com.rincliu.xreader.data.model.rss.RSSData;
import com.rincliu.xreader.data.model.rss.RSSSource;
import com.rincliu.xreader.util.UiUtil;

public class LaunchActivity extends BaseActivity {
    private ArrayList<RSSSource> sourceList;

    private boolean isCanceled = false;

    private RSSData mData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        sourceList = ReaderDBHelper.getInstance(this).getSourceList(null);
        if (sourceList != null && sourceList.size() > 0) {
            readSource(sourceList.get(0));
        } else {
            UiUtil.toast(this, R.string.read_data_source_failed, true);
            jump();
        }
        TextView tvCopyright = (TextView) findViewById(R.id.tv_copyright1);
        int year = Calendar.getInstance().get(Calendar.YEAR);
        tvCopyright.setText(String.format(getString(R.string.copyright1), year));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isCanceled = true;
    }

    private void readSource(final RSSSource source) {
        RSSReader.getInstance(this).read(source.getUrl(), new ReadCallback() {
            @Override
            public void onSuccess(RSSData data) {
                mData = data;
                jump();
            }

            @Override
            public void onFailure() {
                UiUtil.toast(getApplicationContext(), R.string.read_data_failed, true);
                jump();
            }
        });
    }

    private void jump() {
        if (isCanceled) {
            return;
        }
        Intent intent = new Intent(this, ReaderActivity.class);
        intent.putExtra(ReaderActivity.EXTRA_SOURCE_LIST, sourceList);
        intent.putExtra(ReaderActivity.EXTRA_DATA, mData);
        startActivity(intent);
        finish();
    }
}
