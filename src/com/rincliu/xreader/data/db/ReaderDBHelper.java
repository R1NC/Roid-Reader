package com.rincliu.xreader.data.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.rincliu.xreader.R;
import com.rincliu.xreader.data.model.rss.RSSSource;

public class ReaderDBHelper extends BaseDBHelper {

    private static final String DB_NAME = "DB_READER";

    private static final int DB_VERSION = 1;

    private static final String TAB_SOURCE = "TAB_SOURCE";

    private static final String TAB_DATA = "TAB_DATA";

    private static final String COL_URL = "COL_URL";

    private static final String COL_DESC = "COL_DESC";

    private static final String COL_XML = "COL_XML";

    private static final String[] SOURCE_URL_ARRAY = {"http://www.36kr.com/feed/", "http://www.infoq.com/cn/feed",
            "http://www.ifanr.com/feed", "http://www.leiphone.com/feed", "http://livesino.net/feed",
            "http://www.wpdang.com/feed", "http://www.kuailiyu.com/feed/", "http://www.pingwest.com/feed/",
            "http://techcrunch.cn/feed/", "http://www.199it.com/feed", "http://www.programmer.com.cn/feed/",
            "http://feeds.geekpark.net/", "http://feed.evolife.cn/", "http://cn.engadget.com/rss.xml",
            "http://www.dgtle.com/rss/dgtle.xml", "http://www.huxiu.com/rss/0.xml",
            "http://www.tmtpost.com/?feed=rss2&cat=-1204",
            "http://tech2ipo.feedsportal.com/c/34822/f/641707/index.rss", "http://rss.aqee.net/",
            "http://feed.feedsky.com/pcdigest", "http://feed.williamlong.info/", "http://www.iterduo.com/feed",
            "http://www.yankeji.cn/feed", "http://cn.technode.com/feed/"};

    private final Context context;

    private static ReaderDBHelper helper;

    public static synchronized ReaderDBHelper getInstance(Context context) {
        if (helper == null) {
            helper = new ReaderDBHelper(context);
        }
        return helper;
    }

    private ReaderDBHelper(Context context) {
        super(context, DB_NAME, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_TAB_SOURCE = "CREATE TABLE " + TAB_SOURCE + " (" + COL_URL + " TEXT PRIMARY KEY, " + COL_DESC
                + " TEXT);";
        String SQL_CREATE_TAB_DATA = "CREATE TABLE " + TAB_DATA + " (" + COL_URL + " TEXT PRIMARY KEY, " + COL_XML
                + " TEXT);";
        db.execSQL(SQL_CREATE_TAB_SOURCE);
        String[] sourceTitleArray = context.getResources().getStringArray(R.array.source_title_array);
        for (int i = 0; i < SOURCE_URL_ARRAY.length; i++) {
            ContentValues values = new ContentValues();
            values.put(COL_URL, SOURCE_URL_ARRAY[i]);
            values.put(COL_DESC, sourceTitleArray[i]);
            doInsert(db, TAB_SOURCE, values);
        }
        db.execSQL(SQL_CREATE_TAB_DATA);
    }

    /**
     * [description]
     * 
     * @param url
     * @param desc
     * @see []
     */
    public void putSource(String url, String desc) {
        SQLiteDatabase db = getWritableDatabase();
        if (db != null) {
            Cursor cursor = db.query(TAB_SOURCE, new String[] {COL_URL, COL_DESC}, COL_URL + "=?", new String[] {url},
                    null, null, null);
            if (cursor != null) {
                ContentValues values = new ContentValues();
                values.put(COL_URL, url);
                values.put(COL_DESC, desc);
                if (cursor.getCount() > 0) {
                    doUpdate(db, TAB_SOURCE, values, COL_URL + "=?", new String[] {url});
                } else {
                    doInsert(db, TAB_SOURCE, values);
                }
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
            if (db.isOpen()) {
                db.close();
            }
        }
    }

    public boolean hasSource(String url) {
        boolean has = false;
        SQLiteDatabase db = getWritableDatabase();
        if (db != null) {
            Cursor cursor = db.query(TAB_SOURCE, new String[] {COL_URL, COL_DESC}, COL_URL + "=?", new String[] {url},
                    null, null, null);
            if (cursor != null) {
                has = cursor.moveToNext();
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
            if (db.isOpen()) {
                db.close();
            }
        }
        return has;
    }

    /**
     * [description]
     * 
     * @param url
     * @see []
     */
    public void removeSource(String url) {
        SQLiteDatabase db = getWritableDatabase();
        if (db != null) {
            Cursor cursor = db.query(TAB_SOURCE, new String[] {COL_URL, COL_DESC}, COL_URL + "=?", new String[] {url},
                    null, null, null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    doDelete(db, TAB_SOURCE, COL_URL + "=?", new String[] {url});
                }
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
            if (db.isOpen()) {
                db.close();
            }
        }
    }

    /**
     * [description]
     * 
     * @return
     * @see []
     */
    public ArrayList<RSSSource> getSourceList(String keyWords) {
        ArrayList<RSSSource> sourceList = null;
        SQLiteDatabase db = getReadableDatabase();
        if (db != null) {
            String selection = null;
            String[] selectionArgs = null;
            if (!TextUtils.isEmpty(keyWords)) {
                selection = COL_DESC + " LIKE ?";
                selectionArgs = new String[] {"%" + keyWords + "%"};
            }
            Cursor cursor = db.query(TAB_SOURCE, new String[] {COL_URL, COL_DESC}, selection, selectionArgs, null,
                    null, null);
            if (cursor != null) {
                sourceList = new ArrayList<RSSSource>();
                while (cursor.moveToNext()) {
                    RSSSource source = new RSSSource();
                    source.setUrl(cursor.getString(cursor.getColumnIndex(COL_URL)));
                    source.setDesc(cursor.getString(cursor.getColumnIndex(COL_DESC)));
                    sourceList.add(source);
                }
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
            if (db.isOpen()) {
                db.close();
            }
        }
        return sourceList;
    }

    /**
     * [description]
     * 
     * @param url
     * @param xml
     * @see []
     */
    public void putData(String url, String xml) {
        SQLiteDatabase db = getWritableDatabase();
        if (db != null) {
            Cursor cursor = db.query(TAB_DATA, new String[] {COL_URL, COL_XML}, COL_URL + "=?", new String[] {url},
                    null, null, null);
            if (cursor != null) {
                ContentValues values = new ContentValues();
                values.put(COL_URL, url);
                values.put(COL_XML, xml);
                if (cursor.getCount() > 0) {
                    doUpdate(db, TAB_DATA, values, COL_URL + "=?", new String[] {url});
                } else {
                    doInsert(db, TAB_DATA, values);
                }
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
            if (db.isOpen()) {
                db.close();
            }
        }
    }

    /**
     * [description]
     * 
     * @param url
     * @return
     * @see []
     */
    public String getData(String url) {
        String xml = null;
        SQLiteDatabase db = getReadableDatabase();
        if (db != null) {
            Cursor cursor = db.query(TAB_DATA, new String[] {COL_URL, COL_XML}, COL_URL + "=?", new String[] {url},
                    null, null, null);
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    xml = cursor.getString(cursor.getColumnIndex(COL_XML));
                }
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
            if (db.isOpen()) {
                db.close();
            }
        }
        return xml;
    }

    /**
     * [description]
     * 
     * @param url
     * @see []
     */
    public void removeData(String url) {
        SQLiteDatabase db = getWritableDatabase();
        if (db != null) {
            Cursor cursor = db.query(TAB_DATA, new String[] {COL_URL, COL_XML}, COL_URL + "=?", new String[] {url},
                    null, null, null);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    doDelete(db, TAB_DATA, COL_URL + "=?", new String[] {url});
                }
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
            if (db.isOpen()) {
                db.close();
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST" + TAB_DATA);
        db.execSQL("DROP TABLE IF EXIST" + TAB_SOURCE);
        onCreate(db);
    }

    public void registerSourceContentObserver(ContentObserver observer) {
        registerContentObserver(TAB_SOURCE, observer);
    }

    public void registerDataContentObserver(ContentObserver observer) {
        registerContentObserver(TAB_DATA, observer);
    }

}
