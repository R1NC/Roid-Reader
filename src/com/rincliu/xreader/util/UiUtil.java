package com.rincliu.xreader.util;

import com.rincliu.xreader.R;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class UiUtil {
    /**
     * @param context
     * @param msg
     * @param isLong
     */
    public static void toast(Context context, String msg, boolean isLong) {
        Toast toast;
        toast = new Toast(context);
        View view = LayoutInflater.from(context).inflate(R.layout.toast, null);
        TextView tv = (TextView) view.findViewById(R.id.tv_msg);
        tv.setText(msg);
        toast.setView(view);
        toast.setDuration(isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /**
     * @param context
     * @param msg
     */
    public static void toast(Context context, String msg) {
        toast(context, msg, false);
    }

    /**
     * @param context
     * @param strResId
     * @param isLong
     */
    public static void toast(Context context, int strResId, boolean isLong) {
        toast(context, context.getString(strResId), isLong);
    }

    /**
     * @param context
     * @param strResId
     */
    public static void toast(Context context, int strResId) {
        toast(context, context.getString(strResId));
    }
}
