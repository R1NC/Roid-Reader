package com.rincliu.xreader.data.model.rss;

import java.util.ArrayList;

import com.rincliu.xreader.data.model.BaseData;

public class RSSData extends BaseData {
    private static final long serialVersionUID = -5212149025948161001L;

    private ArrayList<RSSEntity> itemList;

    public ArrayList<RSSEntity> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<RSSEntity> itemList) {
        this.itemList = itemList;
    }
}
