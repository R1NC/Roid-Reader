package com.rincliu.xreader.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.rincliu.xreader.R;
import com.rincliu.xreader.app.BaseActivity;
import com.rincliu.xreader.data.model.rss.RSSEntity;

public class DetailActivity extends BaseActivity
{
    public static final String EXTRA_DATA = "extra_data";
    
    private static final String URL_BLANK = "about:blank";
    
    private static final String MIME_HTML = "text/html";
    
    private static final String CHARSET_UTF8 = "utf-8";

    private RSSEntity item;

    private WebView wvContent;

    private ImageView ivGoBack, ivGoForward, ivRefresh;

    private boolean isGoBack;

    @SuppressLint("SetJavaScriptEnabled")
    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        item = (RSSEntity) getIntent().getSerializableExtra(EXTRA_DATA);
        setTitle(item.getTitle());
        ivGoBack = (ImageView) findViewById(R.id.iv_go_back);
        ivGoBack.setOnClickListener(this);
        ivGoForward = (ImageView) findViewById(R.id.iv_go_forward);
        ivGoForward.setOnClickListener(this);
        ivRefresh = (ImageView) findViewById(R.id.iv_refresh);
        ivRefresh.setOnClickListener(this);
        wvContent = (WebView) findViewById(R.id.wv_content);
        WebSettings ws=wvContent.getSettings();
        ws.setAppCacheEnabled(true);
        ws.setPluginState(WebSettings.PluginState.ON);
        ws.setJavaScriptEnabled(true);
        wvContent.setWebViewClient(mWebViewClient);
        wvContent.setWebChromeClient(mWebChromeClient);

        wvContent.getSettings().setBlockNetworkLoads(false);
        loadOriginalData();
    }

    private final WebViewClient mWebViewClient = new WebViewClient()
    {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error)
        {
            handler.proceed();
            updateState();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            view.getSettings().setBlockNetworkImage(true);
            findViewById(R.id.pb_loading).setVisibility(View.VISIBLE);
            updateState();
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            view.getSettings().setBlockNetworkImage(false);
            findViewById(R.id.pb_loading).setVisibility(View.GONE);
            if (isGoBack && URL_BLANK.equals(url))
            {
                loadOriginalData();
                isGoBack = false;
            }
            updateState();
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
        {
            super.onReceivedError(view, errorCode, description, failingUrl);
            findViewById(R.id.pb_loading).setVisibility(View.GONE);
            updateState();
        }

        @Override
        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload)
        {
            super.doUpdateVisitedHistory(view, url, isReload);
            updateState();
        }
    };

    private final WebChromeClient mWebChromeClient = new WebChromeClient()
    {
        @Override
        public void onProgressChanged(WebView view, int newProgress)
        {
            super.onProgressChanged(view, newProgress);
        }

        @Override
        public void onReceivedTitle(WebView view, String title)
        {
            super.onReceivedTitle(view, title);
            setTitle(title);
        }

        @Override
        public void onReceivedIcon(WebView view, Bitmap icon)
        {
            super.onReceivedIcon(view, icon);
        }
    };

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        wvContent.getSettings().setBlockNetworkImage(true);
        wvContent.getSettings().setBlockNetworkLoads(true);
        wvContent.destroy();
    }

    @Override
    public void onClick(View view)
    {
        super.onClick(view);
        switch (view.getId())
        {
            case R.id.iv_go_back:
                if (wvContent.canGoBack())
                {
                    wvContent.goBack();
                    isGoBack = true;
                }
                break;
            case R.id.iv_go_forward:
                if (wvContent.canGoForward())
                {
                    wvContent.goForward();
                }
                break;
            case R.id.iv_refresh:
                if (URL_BLANK.equals(wvContent.getUrl()))
                {
                    loadOriginalData();
                }
                else
                {
                    wvContent.reload();
                }
                break;
        }
    }
    
    private void loadOriginalData(){
        wvContent.loadDataWithBaseURL(null, item.getContent(), MIME_HTML, CHARSET_UTF8, null);
    }

    private void updateState()
    {
        if (wvContent.canGoBack())
        {
            ivGoBack.setEnabled(true);
        }
        else
        {
            ivGoBack.setEnabled(false);
        }
        if (wvContent.canGoForward())
        {
            ivGoForward.setEnabled(true);
        }
        else
        {
            ivGoForward.setEnabled(false);
        }
    }
}
